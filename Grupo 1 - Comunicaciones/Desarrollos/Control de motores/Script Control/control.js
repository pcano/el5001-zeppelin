/*	EL5002-2 Introducción a Taller de Diseño
	Script de control de pines de propósito general usando la librería WebIOPi con Javascript.
	Joaquín Chávez, Francisco Villa.
*/
/*	OJO!!!!: 	Las funciones de encendido de los pines son de referencia y seguramente deben ser
				modificadas según los requerimientos del dirigible. (PWM, 0/1, etc)
				Info. sobre el uso de estas funciones en la documentación de WebIOPi.
*/

//Función que crea el objeto button y lo vincula con las funciones "mousedown" y "mouseup"
function makeMUMDButton(id,label){
	var button = $('<button type="button" class="Default">');
	button.attr("id", id);
	button.text(label);
	button.bind("mousedown", mousedown(id));
	button.bind("mouseup", mouseup(id));
	return button;
}

//Esta no estoy seguro de qué es lo que hace pero aparecía en el ejemplo que seguí de guía.
webiopi().ready(function() { 
		webiopi().refreshGPIO(true);
		var content, button;
		content = $("#content");
		button = webiopi().createGPIOButton(21,"21");
		content.append(button);
		button = webiopi().createGPIOButton(22,"22");
		content.append(button);
		button = webiopi().createGPIOButton(10,"10");
		content.append(button);
		button = webiopi().createGPIOButton(9,"9");
		content.append(button);
		button = webiopi().createGPIOButton(11,"11");
		content.append(button); 
	} 
);

//Función que define qué hacer cuando se presiona cada uno de los botones de la página con el mouse.
function mousedown(dir){
	switch(dir){ 
	
	case 1: //Si es adelante. Encender los dos motores. 
		stopall(); //Esta funcion apaga todos los motores antes de efectuar la nueva instrucción.
		webiopi().digitalWrite(0,1); //Deja en valor 1 lógico el PIN0
		webiopi().digitalWrite(4,1); //Deja en valor 1 lógico el PIn4
		$('.forward').css('border-bottom','45px solid green'); //Esto cambia el la propiedad CSS del botón para que se vea verde cuando se presiona.
		break; //Se sale del switch. El resto es análogo.
	
	case 2: //Si es a la izq. Hacer lo necesario. (Falta definir bien como se generarán los movimientos y velocidades)
		stopall();
		webiopi().digitalWrite(1,1); 
		webiopi().digitalWrite(17,1);
		$('.left').css('border-bottom','45px solid green');
		break;
		//Lo que sigue es análogo.
	case 3: 
		stopall();
		webiopi().digitalWrite(1,1); 
		webiopi().digitalWrite(4,1); 
		$('.backward').css('border-bottom','45px solid green');
		break;
	
	case 4:
		stopall(); // all off 
		webiopi().digitalWrite(0,1);
		webiopi().digitalWrite(17,1); 
		$('.right').css('border-bottom','45px solid green');
		break;
		
	case 5:
		stopall();
		//Señal para subir.
		$('.up').css('background-color','green');
		break;	
		
	case 6:
		stopall();
		//Señal para bajar.
		$('.down').css('background-color','green');
		break;
		
	case 7:
		//Alguna rutina de emergencia.
		stopall();
		$('.stop').css('background-color','#8A0808');
		break;
	
	default:
		mouseup(0); // just stop
	} 
}

//Esta función se encarga de apagar todos los motores una vez que se dejan de presionar los botones.
//El funcionamiento es análogo a las funciones anteriores.
function mouseup(dir){
	switch(dir){
	case 1:
		$('.forward').css('border-bottom','45px solid #39839B');
		stopall();
		break;
	case 2:
		$('.left').css('border-bottom','45px solid #39839B');
		stopall();
		break;
	case 3:
		$('.backward').css('border-bottom','45px solid #39839B');
		stopall();
		break;
	case 4:
		$('.right').css('border-bottom','45px solid #39839B');
		stopall();
		break;
	case 5:
		$('.up').css('background-color','#39839B');
		stopall();
		break;
	case 6:
		stopall();
		$('.down').css('background-color','#39839B');
		break;
	case 7:
		$('.stop').css('background-color','red');
		stopall();
		break;	
	}
}

//Función que define qué hacer cuando se mantiene presionada una tecla del teclado.
function teclasdown(e){
	switch(e.keyCode){
	case 37: //Flecha izq.
		mousedown(2);
		break;
	case 38: //Flecha arriba
		mousedown(1);
		break;
	case 39: //Flecha der.
		mousedown(4);
		break;
	case 40: //Flecha abajo
		mousedown(3);
		break;
	case 65: //Tecla A.
		mousedown(5);
		break;
	case 90: //Tecla Z
		mousedown(6);
		break;
	case 32: //Barra espaciadora
		mousedown(7);
		break;

	}
}

//Función que define qué hacer cuando se deja de presionar una tecla.
function teclasup(e){
	switch(e.keyCode){
	case 37:
		mouseup(2);
		break;
	case 38:
		mouseup(1);
		break;
	case 39:
		mouseup(4);
		break;
	case 40:
		mouseup(3);
		break;
	case 32:
		mouseup(7);	
	case 65:
		mouseup(5);
		break;
	case 90:
		mouseup(6);
		break;
	}
}

//Función que deja en 0 todos los pines listados.
function stopall(){
	webiopi().digitalWrite(0,0);
	webiopi().digitalWrite(1,0); 
	webiopi().digitalWrite(4,0); 
	webiopi().digitalWrite(17,0); 
}