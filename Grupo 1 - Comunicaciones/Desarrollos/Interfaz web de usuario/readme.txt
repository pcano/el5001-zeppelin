Para implementar la interfaz de usuario se debe instalar la Librería WebIOPi en el dispositivo Raspberry Pi.

Luego de esto, se deben insertar los siguientes archivos y carpetas:
	- img (carpeta de imágenes)
	- index.html
	- style.css
	- control.js (este archivo se encuentra en la carpeta de control de motores del repositorio)
	
dentro de la carpeta WebIOPi-0.6.0/htdocs ubicada en el dispositivo Raspberry.
